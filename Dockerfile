FROM python:3.9-alpine3.12

# Print all logs without buffering it.
ENV PYTHONUNBUFFERED 1

# This port will be used by gunicorn.
EXPOSE 8080

# Create app dir and install requirements.
RUN mkdir /opt/ajc
WORKDIR /opt/ajc

COPY requirements.txt ./

RUN python3 -m venv venv && \
    venv/bin/pip install --upgrade pip && \
    venv/bin/pip install wheel==0.36.2 && \
    venv/bin/pip install -r requirements.txt --no-cache-dir

# Copy project
COPY . ./
RUN chmod +x boot.sh cron.sh

# add cronjob
RUN echo "0 5 * * * /opt/ajc/cron.sh" >> /etc/crontabs/root

# run it
ENTRYPOINT ["/opt/ajc/boot.sh"]