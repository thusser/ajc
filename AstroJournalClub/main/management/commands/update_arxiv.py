from django.core.management.base import BaseCommand
import feedparser
import re
from lxml import html
from AstroJournalClub.main import models
from datetime import datetime


class Command(BaseCommand):
    help = 'Update from arxiv.org'

    #def add_arguments(self, parser):
    #    parser.add_argument('files', type=str, nargs='+', help='Names of files to ingest')

    def handle(self, *args, **options):
        # get and parse RSS feed
        feed = feedparser.parse('https://rss.arxiv.org/rss/astro-ph/')

        # loop all entries
        for number, entry in enumerate(feed['entries'], 1):
            # extract identifier, title and summary
            identifier = entry.id[entry.id.rfind(':') + 1:]
            title = entry.title[:entry.title.find('(arXiv:')]

            # get/create publication
            date = datetime.now()
            publication, _ = models.Publication.objects.get_or_create(identifier=identifier, date=date,
                                                                      defaults={'title': title, 'number': number,
                                                                                'url': entry.link,
                                                                                'summary': entry.summary})

            # add category
            category, _ = models.Category.objects.get_or_create(name=entry.category)
            publication.categories.add(category)
            publication.save()

            # extract authors
            for order, a in enumerate(entry.authors[0]['name'].split(',')):
                author, _ = models.Author.objects.get_or_create(name=a.strip())
                publication.add_author(order, author)

