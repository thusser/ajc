#!/bin/sh
source venv/bin/activate

echo "Starting cron"
crontab -l
crond
echo "Done"

echo "Updating database"
python manage.py migrate
python manage.py collectstatic --noinput
echo "Done"

exec gunicorn -b :8080 --access-logfile - --error-logfile - --log-level INFO AstroJournalClub.wsgi